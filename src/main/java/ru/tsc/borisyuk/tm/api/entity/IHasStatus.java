package ru.tsc.borisyuk.tm.api.entity;

import ru.tsc.borisyuk.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
