package ru.tsc.borisyuk.tm.repository;

import ru.tsc.borisyuk.tm.api.repository.IUserRepository;
import ru.tsc.borisyuk.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(String login) {
        for (final User user : entities) {
            if (login.equals(user.getLogin()))
                return user;
        }
        return null;
    }

    @Override
    public User findByEmail(String email) {
        for (final User user : entities) {
            if (email.equals(user.getEmail()))
                return user;
        }
        return null;
    }

    @Override
    public User removeById(String id) {
        final User user = findById(id);
        if (user == null) return null;
        entities.remove(user);
        return user;
    }

    @Override
    public User removeByLogin(String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        entities.remove(user);
        return user;
    }

}
