package ru.tsc.borisyuk.tm.api.service;

public interface IServiceLocator {

    ITaskService getTaskService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ICommandService getCommandService();

    IAuthService getAuthService();

    IUserService getUserService();

}
