package ru.tsc.borisyuk.tm.command.project;

import ru.tsc.borisyuk.tm.command.AbstractProjectCommand;
import ru.tsc.borisyuk.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public class ProjectRemoveWithAllTasksById extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-remove-with-tasks-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove project with tasks by id...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (serviceLocator.getProjectService().findById(userId, projectId) == null)
            throw new ProjectNotFoundException();
        serviceLocator.getProjectTaskService().removeById(userId, projectId);
    }


}
