package ru.tsc.borisyuk.tm.repository;

import ru.tsc.borisyuk.tm.api.repository.IOwnerRepository;
import ru.tsc.borisyuk.tm.model.AbstractOwnerEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {

    @Override
    public void add(final String userId, final E entity) {
        if (userId.equals(entity.getUserId())) {
            entities.add(entity);
        }
    }

    @Override
    public void remove(final String userId, final E entity) {
        if (!userId.equals(entity.getUserId()))
            entities.remove(entity);
    }

    @Override
    public List<E> findAll(final String userId) {
        final List<E> result = new ArrayList<>();
        for (E entity : entities) {
            if (userId.equals(entity.getUserId()))
                result.add(entity);
        }
        return result;
    }

    @Override
    public List<E> findAll(final String userId, Comparator<E> comparator) {
        final List<E> listOfEntities = new ArrayList<>();
        for (E entity : entities) {
            if (userId.equals(entity.getUserId()))
                listOfEntities.add(entity);
        }
        listOfEntities.sort(comparator);
        return listOfEntities;
    }

    @Override
    public void clear(final String userId) {
        final List<E> listOfProjects = findAll(userId);
        entities.removeAll(listOfProjects);
    }

    @Override
    public E findById(final String userId, final String id) {
        for (E entity : entities) {
            if (!userId.equals(entity.getUserId())) continue;
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E findByIndex(final String userId, final Integer index) {
        final List<E> listOfEntities = findAll(userId);
        return listOfEntities.get(index);
    }

    @Override
    public E removeById(final String userId, final String id) {
        final E entity = findById(userId, id);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

    @Override
    public E removeByIndex(final String userId, final Integer index) {
        final E entity = findByIndex(userId, index);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        final E entity = findById(userId, id);
        return entity != null;
    }

    @Override
    public boolean existsByIndex(final String userId, final Integer index) {
        final E entity = findByIndex(userId, index);
        return entity != null;
    }

}
