package ru.tsc.borisyuk.tm;

import ru.tsc.borisyuk.tm.component.Bootstrap;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}