package ru.tsc.borisyuk.tm.command.system;

import ru.tsc.borisyuk.tm.command.AbstractCommand;

public class DeveloperInfoShowCommand extends AbstractCommand {

    @Override
    public String name() {
        return "about";
    }

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String description() {
        return "Display developer info...";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Borisyuk Dmitriy");
        System.out.println("E-MAIL: Kytyzoff92work@gmail.com");
    }

}
